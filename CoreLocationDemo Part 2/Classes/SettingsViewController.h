//
//  SettingsViewController.h
//  CoreLocationDemo
//
//  Created by David on 1/11/14.
//
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController


- (void) setMainScreenPointer: (UIViewController*)mainScreen;
- (UIViewController*) getMainScreenPointer;

- (IBAction)openMainScreen:(id)sender;



@property (nonatomic, retain, readonly)UIViewController* m_pMainScreen;

@end


