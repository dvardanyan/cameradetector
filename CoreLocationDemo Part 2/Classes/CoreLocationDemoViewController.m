//
//  CoreLocationDemoViewController.m
//  CoreLocationDemo
//
//  Created by Nicholas Vellios on 8/15/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "CoreLocationDemoViewController.h"
#import "SettingsViewController.h"

const NSString *JSON_COORD_STR = @"coords";
const NSString *JSON_NAME_STR  = @"name";
const NSString *JSON_LIMIT_STR = @"limit";
const NSString *JSON_TYPE_STR  = @"type";


@implementation CoreLocationDemoViewController

@synthesize CLController;

- (void)viewDidLoad {
    [super viewDidLoad];
	m_pSettingsViewCntrl = nil;
    
    soundVolume = 0.1;
	CLController = [[CoreLocationController alloc] init];
	CLController.delegate = self;
    camerasLocations = [[NSMutableArray alloc] init];
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tire.jpg"]];
    
    
    [self fillCameraLocations];
    self->beepSound  = 	[self createSoundID: @"Beep.aiff"];
    [self initSounds];
    
	[CLController.locMgr startUpdatingLocation];
}

- (void)locationUpdate:(CLLocation *)location {
	
    
    @try {
        double speedMeterSec = [location speed];
        double speedKmHrs = 3.6 * speedMeterSec;
        
        speedLabel.text = [NSString stringWithFormat:@"SPEED in Km/Hrs: %f", speedKmHrs];
        
        latitudeLabel.text = [NSString stringWithFormat:@"LATITUDE: %f", location.coordinate.latitude];
        longitudeLabel.text = [NSString stringWithFormat:@"LONGITUDE: %f", location.coordinate.longitude];
        altitudeLabel.text = [NSString stringWithFormat:@"ALTITUDE: %f", [location altitude]];
        
        if([self isCameraNearBy:location])
        {
            //AudioServicesPlaySystemSound( self->beepSound);
            [audioPlayer play];

            altitudeLabel.text = [NSString stringWithFormat:@"CAMERA KA APER!!! KAMAC QSHI"];
        }
    }
    @catch (NSException *exception) {
         NSLog(@"Exception: %@", exception);
    }
    
}

- (void)locationError:(NSError *)error {
	speedLabel.text = [error description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return YES;
}

- (bool) isCameraNearBy:(CLLocation*) location{
    //TODO: review the code to understand "nearby" cameras
    for (int i = 0; i< camerasLocations.count; ++i) {
        CLLocation* cameraLocation = [camerasLocations objectAtIndex:i];
        CLLocationDistance distance = [cameraLocation distanceFromLocation:location];
        
        // Note that distance is mesured in meters
        if (distance < 100)
        {
            return true;
        }
    }
    return false;
}

- (void)initSounds{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Beep.aiff", [[NSBundle mainBundle] resourcePath]]];
	
	NSError *error;
	audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
	audioPlayer.numberOfLoops = -1;
    [audioPlayer setVolume:  soundVolume]; // volume of the sound
	
	if (audioPlayer == nil)
		NSLog(@"Exception: %@",  [error description]);
}

- (IBAction)openSettings:(id)sender {
    
    // Show the setting to change the volume of the sound, write email to developer.....
    NSLog(@" click");
    
    if( nil == self->m_pSettingsViewCntrl  )
    {
        SettingsViewController *settingsViewCntrl = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        [settingsViewCntrl setMainScreenPointer:self];
        
        self->m_pSettingsViewCntrl = settingsViewCntrl;
        [settingsViewCntrl release];
        
    }
    
    
    [self presentViewController:self->m_pSettingsViewCntrl animated:YES completion:nil];
    
    //MyViewController *myViewController = [[MyViewController alloc] initWithNibName:nil bundle:nil];
    
   // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:myViewController];
    //now present this navigation controller as modally
    //[self presentModalViewController:navigationController YES];
    
    
    
    
    
    
    //UIViewController* flipViewController = [[UIViewController alloc] initWithNibName:@"flip" bundle:[NSBundle mainBundle]];
    //[self.view addSubview:flipViewController.view];

}

- (SystemSoundID)createSoundID:(NSString *)name{
    
    NSString *path = [NSString stringWithFormat: @"%@/%@",
                      [[NSBundle mainBundle] resourcePath], name];
    
    
    NSURL* filePath = [NSURL fileURLWithPath: path isDirectory: NO];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    return soundID;

}

- (void) fillCameraLocations{
    
    @try
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CameraLocations" ofType:@"txt"];
        NSString *cameraLocations = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
        
        self->data = cameraLocations; //[NSString stringWithFormat:@];
        
        NSData * jsonData = [ self->data dataUsingEncoding:NSUTF8StringEncoding];
        NSError *localError = nil;
        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData: jsonData options:0 error:&localError];
    
        if( nil != localError)
        {
            NSLog(@" Error: %@", localError);
            return ;
        }
       
        for(NSString *key in [parsedObject allKeys])
        {
            //NSLog(@"%@",[parsedObject objectForKey:key]);
            NSDictionary *currentCameraInfo = [parsedObject objectForKey : key];
            NSLog(@"name is: %@", [currentCameraInfo objectForKey : JSON_NAME_STR] );
            NSString *cordinatesInfo = [currentCameraInfo objectForKey : JSON_COORD_STR ];
            cordinatesInfo  = [cordinatesInfo substringFromIndex:22];
            double lat = [[[cordinatesInfo substringToIndex:11]  substringFromIndex:1] doubleValue];
            double lon = [[cordinatesInfo substringFromIndex:12] doubleValue];
            CLLocation *cameraLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            [camerasLocations addObject:cameraLocation];
        }
        
        
       // double lat = 37.710;
      //  double lon = -122.450;
        //CLLocation *cameraLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
     //   [camerasLocations addObject:cameraLocation];
    }
    @catch(NSException* ex)
    {
        NSLog(@"Exception: %@", ex);
    }
    
}

- (void)viewDidUnload {
}

- (void)dealloc {
	[CLController release];
    [super dealloc];
}

@end
