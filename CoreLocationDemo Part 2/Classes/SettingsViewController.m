//
//  SettingsViewController.m
//  CoreLocationDemo
//
//  Created by David on 1/11/14.
//
//

#import "SettingsViewController.h"
#import "CoreLocationController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setMainScreenPointer: (UIViewController*)mainScreen
{
    _m_pMainScreen = mainScreen;
}

- (UIViewController*) getMainScreenPointer
{
    assert(_m_pMainScreen);
    return _m_pMainScreen;
}


- (IBAction)openMainScreen:(id)sender {
    
    //return to the main screen
    NSLog(@" click MAIN");
    //CoreLocationController *mainViewCntrl = [[CoreLocationController alloc] initWithNibName:@"CoreLocationDemoViewController" bundle:nil];
    
    assert(_m_pMainScreen);
    [self presentViewController:_m_pMainScreen animated:YES completion:nil];

}
@end
