//
//  main.m
//  OCRApp
//
//  Created by David on 4/7/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OCRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OCRAppDelegate class]));
    }
}
