//
//  SettingsViewController.m
//  CameraDetector
//
//  Created by David on 2/1/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import "SettingsViewController.h"
#import <Social/Social.h> //Facebook/Twitter

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _soundVolumeSlider.value = _soundVolume;
    _warningDistanceSlider.value = _warnDistance;
    [_vibrateNearCameraSwtch setOn:_vibrateNearCamera];
    [_warnUsualCameraSwitch   setOn:_warnUsualCamera];
    [_warnAfterPassingCameraSwitch setOn:_warnAfterPassingCamera];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)contactUsPushed:(id)sender {
    
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = self;
    [mailer setSubject:@"Question/Suggestion"];
    NSArray *usersTo = [NSArray arrayWithObject: @"david.vardanyan90@gmail.com"];
    [mailer setToRecipients:usersTo];
    
    NSString *emailBody =  @"I have following Question/Suggestion:";
    
    [mailer setMessageBody:emailBody isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text
    mailer.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
    
    [self presentViewController:mailer animated:YES completion:nil];
    
}

- (IBAction)postToFacebook:(id)sender {
    //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:@"App that every driver must have in Yerevan!!/n check it in {URL}"];
        [self presentViewController:controller animated:YES completion:Nil];
    //}
}

- (IBAction)postToTwitter:(id)sender {
    
   // if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
   // {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"App that every driver must have in Yerevan!!/n check it in {URL}"];
        [self presentViewController:tweetSheet animated:YES completion:nil];
  //  }
}

- (IBAction)sendSMSToFriend:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = @"SMS message here";
        controller.recipients = [NSArray arrayWithObjects:@"1(234)567-8910", nil];
        controller.messageComposeDelegate = self;
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (IBAction)sendEmailToFriend:(id)sender
{
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = self;
    [mailer setSubject:@"Great App for Drivers in Yerevan"];
    
    NSString *emailBody =  @"I have found cool app for drivers in Yerevan. Check it out :   {URL}";
    
    [mailer setMessageBody:emailBody isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text
    mailer.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
    
    [self presentViewController:mailer animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
            NSLog(@"Error in sending SMS");
			break;
		case MessageComposeResultSent:
            
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"cancel?");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"saved?");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Sent succed");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"sent failue");
            NSLog(@"%@",error);
            break;
        default:
            break;
    }
}



@end
