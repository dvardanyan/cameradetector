//
//  AppDelegate.m
//  WindowbasedTemplate
//
//  Created by David on 1/29/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import "AppDelegate.h"

//Google Maps
#import <GoogleMaps/GoogleMaps.h>
//Rate
#import "Appirater.h"

@implementation AppDelegate

@synthesize firstRun = _firstRun;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //==============
    //Check to see if this is first time app is run by checking flag we set in the defaults
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults objectForKey:@"firstRun"]){
        //flag doesnt exist then this IS the first run
        self.firstRun = TRUE;
        //store the flag so it exists the next time the app starts
        [defaults setObject:[NSDate date] forKey:@"firstRun"];
    }else{
        //flag does exist so this ISNT the first run
        self.firstRun = FALSE;
    }
    //call synchronize to save default - where its saved is managed by iOS - varies by device and iOS/Mac
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
/*    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
 */
    
    // GOOGLE MAP KEY
    [GMSServices provideAPIKey:@"AIzaSyA0377Y_SHdjRvjkyvSr776toquWi8W97s"];
    
    
    [self.window addSubview:[self.navigationController view]];
    [self.window makeKeyAndVisible];
    
    // call the Appirater class
    //[Appirater appLaunched];
    
    [Appirater setAppId:@"552035781"];
    [Appirater setDaysUntilPrompt:1];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:2];
    [Appirater setDebug:YES];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
