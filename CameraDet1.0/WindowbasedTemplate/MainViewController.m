//
//  RootViewController.m
//  CameraDetector
//
//  Created by David on 2/1/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import "MainViewController.h"
#import "Camera.h"
#import <AudioToolbox/AudioServices.h>

#import <GoogleMaps/GoogleMaps.h>

#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>



// JSON parsing keys
const NSString *JSON_COORD_STR = @"coords";
const NSString *JSON_NAME_STR  = @"name";
const NSString *JSON_LIMIT_STR = @"limit";
const NSString *JSON_TYPE_STR  = @"type";

//UserDefaults dictionary keys
NSString *USER_DEFAULTS_WARNSTR_KEY      = @"WarnDistance";
NSString *USER_DEFAULTS_SOUND_VOLUME_KEY = @"SoundVolume";
NSString *USER_DEFAULTS_VIBRATE_NEAR_CAMERA_KEY = @"VibrateNearCamera";
NSString *USER_DEFAULTS_WARN_USUAL_CAMERAS_KEY  = @"WarnForUsualCameras";
NSString *USER_DEFAULTS_WARN_PASSED_CAMERA_KEY  = @"WarnPassedCamera";

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize needleImageView;
@synthesize speedometerCurrentValue;
@synthesize prevAngleFactor;
@synthesize angle;
@synthesize speedometer_Timer;
@synthesize speedometerReading;
@synthesize maxVal;
@synthesize clController;
@synthesize settingsViewController;
@synthesize cameras;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //soundVolume = 0.1;
    cameras =  [[NSMutableArray alloc] init];
    camerasLocations = [[NSMutableArray alloc] init];
    [self fillCameraLocations];
    self->beepSound  = 	[self createSoundID: @"Beep.aiff"];
    [self initSounds];
    
	clController = [[CoreLocationController alloc] init];
	clController.delegate = self;
	[clController.locMgr startUpdatingLocation];
    
    
    // Get Warn Distance from UserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:USER_DEFAULTS_WARNSTR_KEY] )
    {
        warnDistance = 50;
        [defaults setInteger:warnDistance forKey:USER_DEFAULTS_WARNSTR_KEY];
    }
    else
    {
        warnDistance = [defaults integerForKey:USER_DEFAULTS_WARNSTR_KEY];
    }
    
    // get value of vibration option from user defauls
    if(![defaults objectForKey:USER_DEFAULTS_VIBRATE_NEAR_CAMERA_KEY])
    {
        vibrateNearCamera = true;
        [defaults setBool:vibrateNearCamera forKey:USER_DEFAULTS_VIBRATE_NEAR_CAMERA_KEY];
    }
    else
    {
        vibrateNearCamera  = [defaults boolForKey:USER_DEFAULTS_VIBRATE_NEAR_CAMERA_KEY];
    }
    
    // get value of warn for usual cameras
    if (! [defaults objectForKey:USER_DEFAULTS_WARN_USUAL_CAMERAS_KEY]) {
        warnForUsualCameras = true;
    }
    else
    {
        warnForUsualCameras = [defaults boolForKey:USER_DEFAULTS_WARN_USUAL_CAMERAS_KEY];
    }
    

    //get value of warn passed camera
    if (! [defaults objectForKey:USER_DEFAULTS_WARN_PASSED_CAMERA_KEY]) {
        warnPassedCamera = true;
    }
    else{
        warnPassedCamera = [defaults boolForKey:USER_DEFAULTS_WARN_PASSED_CAMERA_KEY];
    }
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //Speedometer staff
    // Add Meter Contents //
	[self addMeterViewContents];

}

- (void)locationUpdate:(CLLocation *)location
{
    
    @try {
        double speedMeterSec = [location speed];
        double speedKmHrs = 3.6 * speedMeterSec;
        
        self.speedometerCurrentValue = speedKmHrs;
        
        speedLabel.text = [NSString stringWithFormat:@"SPEED -  %f Km/Hrs: ", speedKmHrs];
        if(speedKmHrs>40)//es 40@ heto peta poxarinvi @ntacik sahmanvac aragutyunov
        {
            mSpeedStatusbyView.image=[UIImage imageNamed:@"speed_max.jpg"];
            
        }
        else
        {
            mSpeedStatusbyView.image=[UIImage imageNamed:@"speed_min.jpg"];
        }
            
            
        
      //  latitudeLabel.text = [NSString stringWithFormat:@"LATITUDE: %f", location.coordinate.latitude];
      //  longitudeLabel.text = [NSString stringWithFormat:@"LONGITUDE: %f", location.coordinate.longitude];
      //  altitudeLabel.text = [NSString stringWithFormat:@"ALTITUDE: %f", [location altitude]];
        static double previousCameraDistance = 0;
        static bool aproachToCamera = false;
        static bool cameraIsPassed = false;
        double nearestCameraDistance = 0;
        if([self isCameraNearBy:location distance:&nearestCameraDistance])
        {
            // Do not warn if you passed camera
            if (false == warnPassedCamera)
            {
                //TODO: review the numbers to make them constant
                // Also will remove the option for distance from Settings
                // Starting from 30M we start to looking for new camera
                if ( true == cameraIsPassed && nearestCameraDistance >= 30 )
                {
                    cameraIsPassed = false;
                }
            
                if (nearestCameraDistance <= previousCameraDistance)
                {
                    aproachToCamera = true;
                }
                else
                {
                    aproachToCamera = false;
                }
                
                // if device aproaching to the camera and the distance is less then
                // 10 meters then set variable to true
                if (true == aproachToCamera && nearestCameraDistance <= 10)
                {
                    cameraIsPassed = true;
                }
                
            }
            
            // if we don't need to warn passed camera
            if (true == cameraIsPassed && true != warnPassedCamera) {
                return;
            }
           
            
            // vibrate device
            if (vibrateNearCamera) {
                 AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
            
            [audioPlayer play];
            
            distanceFromNearestCamera.text = [NSString stringWithFormat:@"%f M", nearestCameraDistance ];
            previousCameraDistance = nearestCameraDistance;
         //   altitudeLabel.text = [NSString stringWithFormat:@"CAMERA KA APER!!! KAMAC QSHI"];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@", exception);
    }

}

- (void) locationError:(NSError *)error
{
	speedLabel.text = [error description];
}

-(bool) isCameraNearBy: (CLLocation*) location distance: (double*) distance
{
    //TODO: review the code to understand "nearby" cameras
    for (int i = 0; i< cameras.count /*camerasLocations.count*/; ++i) {
        Camera* currentCamera =  [cameras objectAtIndex:i];
        
        CLLocation* cameraLocation = [currentCamera location]; //[camerasLocations objectAtIndex:i];
        CLLocationDistance clDistance = [cameraLocation distanceFromLocation:location];
        
        //TODO: REVIEW the LOGIC!!!
        // Note that distance is mesured in meters
        if (clDistance < warnDistance )
        {
            if (false != warnForUsualCameras && UsualCamera == currentCamera.type)
            {
                continue;
            }
            *distance = clDistance;
            return true;
        }
    }
    return false;
}

- (void) initSounds
{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Beep.aiff", [[NSBundle mainBundle] resourcePath]]];
	
	NSError *error;
	audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
	audioPlayer.numberOfLoops = -1;
    
    // Get Sound volume from UserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:USER_DEFAULTS_SOUND_VOLUME_KEY] )
    {
        //there are no any object for SoundVolume, so set any
        // and save it in the memory
        soundVolume = 0.3;
        [defaults setDouble:soundVolume forKey:USER_DEFAULTS_SOUND_VOLUME_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else  // this is second lunch of application get saved value
    {
        soundVolume =[defaults doubleForKey:USER_DEFAULTS_SOUND_VOLUME_KEY];
    }
    
    [audioPlayer setVolume:  soundVolume]; // volume of the sound
    
	
	if (audioPlayer == nil)
		NSLog(@"Exception: %@",  [error description]);
}

- (IBAction)onOffSwitchValueChanged:(id)sender {
    
    if (onOffSwitch.isOn) {
        [clController.locMgr startUpdatingLocation];
    }
    else
    {
        [clController.locMgr stopUpdatingLocation];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showSettingsDilaog"])
    {
        //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        SettingsViewController *settViewController = segue.destinationViewController;
        settViewController.soundVolume = soundVolume;
        settViewController.warnDistance = warnDistance;
        settViewController.vibrateNearCamera = vibrateNearCamera;
        settViewController.warnUsualCamera   = warnForUsualCameras;
        settViewController.warnAfterPassingCamera = warnPassedCamera;
        
    }
    
    if ([segue.identifier isEqualToString:@"showMapDialog"])
    {
        MapViewController* mapViewCntrler = segue.destinationViewController;
        mapViewCntrler.cameras = self.cameras;
    }
}

- (SystemSoundID)createSoundID:(NSString *)name
{
    
    NSString *path = [NSString stringWithFormat: @"%@/%@",
                      [[NSBundle mainBundle] resourcePath], name];
    
    
    NSURL* filePath = [NSURL fileURLWithPath: path isDirectory: NO];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    return soundID;
    
}

- (void) fillCameraLocations
{
    @try
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CameraLocations" ofType:@"txt"];
        NSString *cameraLocations = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
        
        self->data = cameraLocations; //[NSString stringWithFormat:@];
        
        NSData * jsonData = [ self->data dataUsingEncoding:NSUTF8StringEncoding];
        NSError *localError = nil;
        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData: jsonData options:0 error:&localError];
        
        if( nil != localError)
        {
            NSLog(@" Error: %@", localError);
            return ;
        }
        
        int count = 0;
        for(NSString *key in [parsedObject allKeys])
        {
            Camera *currentCamera = [Camera alloc];
            NSDictionary *currentCameraInfo = [parsedObject objectForKey : key];
 //           NSLog(@"name is: %@", [currentCameraInfo objectForKey : JSON_NAME_STR] );
            NSString *cordinatesInfo = [currentCameraInfo objectForKey : JSON_COORD_STR ];
            cordinatesInfo  = [cordinatesInfo substringFromIndex:22];
            double lat = [[[cordinatesInfo substringToIndex:11]  substringFromIndex:1] doubleValue];
            double lon = [[cordinatesInfo substringFromIndex:12] doubleValue];
            CLLocation *cameraLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            [camerasLocations addObject:cameraLocation];
            
            currentCamera.cameraNumber = count;
            currentCamera.location = cameraLocation;
            currentCamera.latitude = lat;
            currentCamera.longitude = lon;
            currentCamera.name  = [currentCameraInfo objectForKey:JSON_NAME_STR];
            currentCamera.type = [[currentCameraInfo objectForKey:JSON_TYPE_STR] intValue];
            currentCamera.speedLimit = [[currentCameraInfo objectForKey:JSON_LIMIT_STR] intValue];
            [cameras addObject:currentCamera];
            
            // go ahead!
            ++count;
        }
        
        
        // double lat = 37.710;
        //  double lon = -122.450;
        //CLLocation *cameraLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        //   [camerasLocations addObject:cameraLocation];
    }
    @catch(NSException* ex)
    {
        NSLog(@"Exception: %@", ex);
    }
    
}

- (IBAction)unwindToMainScreen:(UIStoryboardSegue *)segue
{
    SettingsViewController *settViewController = segue.sourceViewController;
    soundVolume = settViewController.soundVolumeSlider.value;
    warnDistance = settViewController.warningDistanceSlider.value;
    vibrateNearCamera = settViewController.vibrateNearCameraSwtch.isOn;
    warnForUsualCameras = settViewController.warnUsualCameraSwitch.isOn;
    warnPassedCamera = settViewController.warnAfterPassingCameraSwitch.isOn;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setDouble:soundVolume forKey:USER_DEFAULTS_SOUND_VOLUME_KEY];
    [defaults setInteger:warnDistance forKey:USER_DEFAULTS_WARNSTR_KEY];
    [defaults setBool:vibrateNearCamera forKey:USER_DEFAULTS_VIBRATE_NEAR_CAMERA_KEY];
    [defaults setBool:warnForUsualCameras forKey:USER_DEFAULTS_WARN_USUAL_CAMERAS_KEY];
    [defaults setBool:warnPassedCamera forKey:USER_DEFAULTS_WARN_PASSED_CAMERA_KEY];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)viewDidUnload
{
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}


/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */






#pragma mark -
#pragma mark Public Methods

-(void) addMeterViewContents
{
	
	UIImageView *backgroundImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320,460)];
	backgroundImageView.image = [UIImage imageNamed:@"main_bg.png"];
	[mSpeedometerView addSubview:backgroundImageView];
	
    CGFloat x = 10;
    CGFloat y = 40;
    
	
	
	UIImageView *meterImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10 - x, 40 - y, 286,315)];
	meterImageView.image = [UIImage imageNamed:@"meter.png"];
	[mSpeedometerView addSubview:meterImageView];
	
	
	
	//  Needle //
	UIImageView *imgNeedle = [[UIImageView alloc]initWithFrame:CGRectMake(143-x,155-y, 22, 84)];
	self.needleImageView = imgNeedle;
		
	self.needleImageView.layer.anchorPoint = CGPointMake(self.needleImageView.layer.anchorPoint.x, self.needleImageView.layer.anchorPoint.y*2);
	self.needleImageView.backgroundColor = [UIColor clearColor];
	self.needleImageView.image = [UIImage imageNamed:@"arrow.png"];
	[mSpeedometerView addSubview:self.needleImageView];
	
	// Needle Dot //
	UIImageView *meterImageViewDot = [[UIImageView alloc]initWithFrame:CGRectMake(131.5-x, 175-y, 45,44)];
	meterImageViewDot.image = [UIImage imageNamed:@"center_wheel.png"];
	[mSpeedometerView addSubview:meterImageViewDot];

	
	// Speedometer Reading //
	UILabel *tempReading = [[UILabel alloc] initWithFrame:CGRectMake(125-x, 250 -y , 60, 30)];
	self.speedometerReading = tempReading;
	
	self.speedometerReading.textAlignment = UITextAlignmentCenter;
	self.speedometerReading.backgroundColor = [UIColor blackColor];
	self.speedometerReading.text= @"0";
	self.speedometerReading.textColor = [UIColor colorWithRed:114/255.f green:146/255.f blue:38/255.f alpha:1.0];
	[mSpeedometerView addSubview:self.speedometerReading ];
	
	// Set Max Value //
	self.maxVal = @"100";
	
	/// Set Needle pointer initialy at zero //
	[self rotateIt:-118.4];
	
	// Set previous angle //
	self.prevAngleFactor = -118.4;
	
	// Set Speedometer Value //
	[self setSpeedometerCurrentValue];
}

#pragma mark -
#pragma mark calculateDeviationAngle Method

-(void) calculateDeviationAngle
{
	
	if([self.maxVal floatValue]>0)
	{
		self.angle = ((self.speedometerCurrentValue *237.4)/[self.maxVal floatValue])-118.4;  // 237.4 - Total angle between 0 - 100 //
	}
	else
	{
		self.angle = 0;
	}
	
	if(self.angle<=-118.4)
	{
		self.angle = -118.4;
	}
	if(self.angle>=119)
	{
		self.angle = 119;
	}
	
	
	// If Calculated angle is greater than 180 deg, to avoid the needle to rotate in reverse direction first rotate the needle 1/3 of the calculated angle and then 2/3. //
	if(abs(self.angle-self.prevAngleFactor) >180)
	{
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5f];
		[self rotateIt:self.angle/3];
		[UIView commitAnimations];
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5f];
		[self rotateIt:(self.angle*2)/3];
		[UIView commitAnimations];
		
	}
	
	self.prevAngleFactor = self.angle;
	
	
	// Rotate Needle //
	[self rotateNeedle];
	
	
}


#pragma mark -
#pragma mark rotateNeedle Method
-(void) rotateNeedle
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5f];
	[self.needleImageView setTransform: CGAffineTransformMakeRotation((M_PI / 180) * self.angle)];
	[UIView commitAnimations];
	
}

#pragma mark -
#pragma mark setSpeedometerCurrentValue

-(void) setSpeedometerCurrentValue
{
	if(self.speedometer_Timer)
	{
		[self.speedometer_Timer invalidate];
		self.speedometer_Timer = nil;
	}
//	self.speedometerCurrentValue = arc4random() % 100; // Generate Random value between 0 to 100. //
	
	self.speedometer_Timer = [NSTimer  scheduledTimerWithTimeInterval:2 target:self selector:@selector(setSpeedometerCurrentValue) userInfo:nil repeats:YES];
	
	self.speedometerReading.text = [NSString stringWithFormat:@"%.2f",self.speedometerCurrentValue];
	
	// Calculate the Angle by which the needle should rotate //
	[self calculateDeviationAngle];
}
#pragma mark -
#pragma mark Speedometer needle Rotation View Methods

-(void) rotateIt:(float)angl
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.01f];
	
	[self.needleImageView setTransform: CGAffineTransformMakeRotation((M_PI / 180) *angl)];
	
	[UIView commitAnimations];
}

@end
