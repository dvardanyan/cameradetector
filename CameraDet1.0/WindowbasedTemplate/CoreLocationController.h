//
//  CoreLocationController.h
//  CameraDetector
//
//  Created by David on 2/3/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol CoreLocationControllerDelegate
@required
- (void)locationUpdate:(CLLocation *)location; // Our location updates are sent here
- (void)locationError:(NSError *)error; // Any errors are sent here
@end


@interface CoreLocationController : NSObject<CLLocationManagerDelegate> {
	//CLLocationManager *locMgr;
	//id delegate;
}

@property (nonatomic, retain) CLLocationManager *locMgr;
@property (assign) id delegate;

@end