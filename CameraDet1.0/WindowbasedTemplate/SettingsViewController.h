//
//  SettingsViewController.h
//  CameraDetector
//
//  Created by David on 2/1/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SettingsViewController : UIViewController <MFMailComposeViewControllerDelegate,
    MFMessageComposeViewControllerDelegate>


@property (weak, nonatomic) IBOutlet UISlider *soundVolumeSlider;

@property (weak, nonatomic) IBOutlet UISlider *warningDistanceSlider;

@property (weak, nonatomic) IBOutlet UISwitch *vibrateNearCameraSwtch;

@property (weak, nonatomic) IBOutlet UISwitch *warnUsualCameraSwitch;

@property (weak, nonatomic) IBOutlet UISwitch *warnAfterPassingCameraSwitch;

@property  (nonatomic) double soundVolume;
@property  (nonatomic) int warnDistance;
@property  (nonatomic) bool vibrateNearCamera;
@property  (nonatomic) bool warnUsualCamera;
@property  (nonatomic) bool warnAfterPassingCamera;


- (IBAction)contactUsPushed:(id)sender;
- (IBAction)postToFacebook:(id)sender;
- (IBAction)postToTwitter:(id)sender;
- (IBAction)sendSMSToFriend:(id)sender;
- (IBAction)sendEmailToFriend:(id)sender;

@end
