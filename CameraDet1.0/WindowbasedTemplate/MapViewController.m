//
//  MapViewController.m
//  CameraDetector
//
//  Created by David on 2/12/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import "MapViewController.h"

#import <GoogleMaps/GoogleMaps.h>

#import "Camera.h"
#import "CustomInfoWindow.h"

@interface MapViewController ()

@end

@implementation MapViewController


@synthesize clController;
@synthesize cameras;

GMSMapView *mapView_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)locateDeviceOnMap:(id)sender {
    CLLocation* location = [mapView_ myLocation];
    GMSCameraPosition *currentPosition =
    [GMSCameraPosition cameraWithLatitude: location.coordinate.latitude
                                longitude: location.coordinate.longitude
                                     zoom: 15];

     [mapView_ setCamera:currentPosition];
}

-(void) drawCamerasOnMap
{
    for (Camera* camera in cameras) {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(camera.latitude, camera.longitude);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        
        //marker.infoWindowAnchor = CGPointMake(0.44f, 0.45f);
        if (SpeedDetector == camera.type ) {
             marker.icon = [UIImage imageNamed:@"SpeedCamera.jpg"];
        }else if ( UsualCamera == camera.type )
        {
            marker.icon = [UIImage imageNamed:@"UsualCamera.jpg"];
        }
            //  marker.title =[NSString stringWithFormat:@"%@/%@/%d",
        //               camera.name,
          //             @"//n",
            //           camera.speedLimit];
        marker.userData = camera;
        marker.map = mapView_;
    }
}

- (IBAction)reportNewCamera:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Report New Camera"
                                                      message:@"Select Camera Type"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Speed Camera", @"Usual Camera", nil];
    [message show];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Speed Camera"])
    {
        NSLog(@"Speed Camera was selected.");
    }
    else if([title isEqualToString:@"Usual Camera"])
    {
        NSLog(@"Usual Camera was selected.");
    }
    else if([title isEqualToString:@"Cancel"])
    {
        NSLog(@"Cancel was selected.");
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    clController = [[CoreLocationController alloc] init];
	clController.delegate = self;
	[clController.locMgr startUpdatingLocation];
    
   // mapView.showsUserLocation = YES;
  //  [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
  //  mapView.delegate = self;
    
    
    // GOOGLE MAPS
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
   GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:40.179611
                                                            longitude:44.516796
                                                                 zoom:15];
  
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
   
   // self.view = mapView_;
    mapView_.frame = self.view.frame;
    [self.view insertSubview:mapView_ atIndex:0];
    mapView_.delegate = self;
    
    [self drawCamerasOnMap];
 /*
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = mapView_;
*/
}

- (UIView *) mapView: (GMSMapView *) mapView markerInfoWindow:(GMSMarker *)marker
{
    CustomInfoWindow* infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"infoWindow" owner:self options:nil] objectAtIndex:0];

    Camera* camera = marker.userData;
    infoWindow.name.text = camera.name;
    //infoWindow.speedLimit.text = [NSString stringWithFormat:@"Limit: %d km/h", camera.speedLimit] ;
    NSString* imageLimitStr = [NSString stringWithFormat:@"%d.jpg", camera.speedLimit];
    infoWindow.speedImage.image =[ UIImage imageNamed:imageLimitStr];
    return  infoWindow;

}

- (void)locationUpdate:(CLLocation *)location
{
  /*  // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = mapView_;
    */
    
    //GMSCameraPosition *newCamera = [GMSCameraPosition
    //                                cameraWithLatitude:location.coordinate.latitude
    //                                longitude:location.coordinate.longitude
    //                                zoom:20];
    //[mapView_ setCamera:newCamera];

    [mapView_ animateToLocation:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];

    
}

- (void) locationError:(NSError *)error
{
	//speedLabel.text = [error description];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
