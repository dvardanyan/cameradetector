//
//  Camera.h
//  CameraDetector
//
//  Created by David on 2/22/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
    SpeedDetector = 1,
    UsualCamera
} CameraType;

@interface Camera : NSObject
{
    double latitude;
    double longitude;
    NSString* name;
    int speedLimit;
    CameraType type;
    CLLocation *location;
    
    int cameraNumber; // unique number to find camera
    
}

@property        double latitude;
@property        double longitude;
@property        NSString* name;
@property        int speedLimit;
@property        CameraType type;
@property        CLLocation* location;


@property        int cameraNumber;

@end
