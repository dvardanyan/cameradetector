//
//  AppDelegate.h
//  WindowbasedTemplate
//
//  Created by David on 1/29/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet UIViewController *navigationController;


//@property (strong, nonatomic) IBOutlet UIWindow *windowOutl;

//flag to denote if this is first time the app is run
@property(nonatomic) BOOL firstRun;

@end
