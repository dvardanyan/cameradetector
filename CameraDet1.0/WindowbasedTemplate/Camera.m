//
//  Camera.m
//  CameraDetector
//
//  Created by David on 2/22/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import "Camera.h"

@implementation Camera

@synthesize type;
@synthesize latitude;
@synthesize longitude;
@synthesize speedLimit;
@synthesize name;
@synthesize location;

@synthesize cameraNumber;



@end
