//
//  RootViewController.h
//  CameraDetector
//
//  Created by David on 2/1/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CoreLocationController.h"
#import <AVFoundation/AVFoundation.h>


#import "SettingsViewController.h"

@interface MainViewController : UIViewController <CoreLocationControllerDelegate> {
    
	CoreLocationController *clController;
	IBOutlet UILabel *speedLabel;
	IBOutlet UILabel *latitudeLabel;
	IBOutlet UILabel *longitudeLabel;
	IBOutlet UILabel *altitudeLabel;
    IBOutlet UILabel *distanceFromNearestCamera;
    __weak IBOutlet UISwitch *onOffSwitch;

    __weak IBOutlet UIImageView *mSpeedStatusbyView;
    __weak IBOutlet UIView *mSpeedometerView;
    
    UIImageView *needleImageView;
    float speedometerCurrentValue;
    float prevAngleFactor;
    float angle;
    NSTimer *speedometer_Timer;
    UILabel *speedometerReading;
    NSString *maxVal;
    
  
    SettingsViewController *settingsViewController;
    
    AVAudioPlayer *audioPlayer;
    // sound of the warning sound
    double soundVolume;
    // container for all camera locations
    NSMutableArray *camerasLocations;
    // container for all cameras & speed detectors
    NSMutableArray *cameras;
    // actual sound which is played when device near cameras
    SystemSoundID beepSound;
    // the distance from device to camera to warn user about "danger"
    int warnDistance;
    // vibrate when you close to the camera
    bool vibrateNearCamera;
    // usual camera nofification
    bool warnForUsualCameras;
    // warn after passing the camera
    bool warnPassedCamera;
    
  
    
    NSString *data;

}
@property (nonatomic, retain) CoreLocationController *clController;
@property (nonatomic, retain) SettingsViewController *settingsViewController;


@property  NSMutableArray *cameras;


@property(nonatomic,retain) UIImageView *needleImageView;
@property(nonatomic,assign) float speedometerCurrentValue;
@property(nonatomic,assign) float prevAngleFactor;
@property(nonatomic,assign) float angle;
@property(nonatomic,retain) NSTimer *speedometer_Timer;
@property(nonatomic,retain) UILabel *speedometerReading;
@property(nonatomic,retain) NSString *maxVal;


-(void) addMeterViewContents;
-(void) rotateIt:(float)angl;
-(void) rotateNeedle;
-(void) setSpeedometerCurrentValue;// fill data from "DB"
-(void) fillCameraLocations;

// will return true if there are camera near locaton, and will return
// distance by parametr
-(bool) isCameraNearBy: (CLLocation*) location distance: (double*) distance;

-(SystemSoundID) createSoundID: (NSString*)name;

-(void) initSounds;

- (IBAction)onOffSwitchValueChanged:(id)sender;


@end

