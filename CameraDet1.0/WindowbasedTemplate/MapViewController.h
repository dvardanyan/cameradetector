//
//  MapViewController.h
//  CameraDetector
//
//  Created by David on 2/12/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocationController.h"
#import <GoogleMaps/GoogleMaps.h>

// TODO: Investigate possible usage of this kit, instead of Google Map LIB
//#import "MapKit/MapKit.h"

@interface MapViewController : UIViewController
<CoreLocationControllerDelegate, GMSMapViewDelegate, UIAlertViewDelegate>
{
    //  IBOutlet MKMapView* mapView;  // <---Google Map LIB
    CoreLocationController *clController;
    
    NSMutableArray *cameras;

}
@property (nonatomic, retain) CoreLocationController *clController;

@property NSMutableArray* cameras;


- (IBAction)locateDeviceOnMap:(id)sender;
-(void) drawCamerasOnMap;

- (IBAction)reportNewCamera:(id)sender;

@end
