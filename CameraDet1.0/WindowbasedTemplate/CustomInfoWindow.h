//
//  CustomInfoWindow.h
//  CameraDetector
//
//  Created by David on 2/23/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomInfoWindow : UIView

@property (weak, nonatomic) IBOutlet UILabel* name;
@property (weak, nonatomic) IBOutlet UILabel* speedLimit;

@property (weak, nonatomic) IBOutlet UIImageView *speedImage;



@end
