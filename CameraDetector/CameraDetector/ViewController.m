//
//  ViewController.m
//  CameraDetector
//
//  Created by David on 1/28/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openSettings:(id)sender {
    
    if ( nil == [self settingsViewCntrler] ) {
        SettingsViewController* settingsViewCntrl = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:[NSBundle mainBundle]];
        self->settingsViewCntrler = settingsViewCntrl;
        //[settingsViewCntrl release];
        
    }

    [self.navigationController pushViewController:self.settingsViewCntrler animated:YES];
}

- (IBAction)testFunction:(id)sender {
    
    if (1 == 1)
    {
    
    }
}
@end
