//
//  ViewController.h
//  CameraDetector
//
//  Created by David on 1/28/14.
//  Copyright (c) 2014 David. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"

@interface ViewController : UIViewController{


   SettingsViewController* settingsViewCntrler;
}

@property (nonatomic, retain) SettingsViewController* settingsViewCntrler;

- (IBAction)openSettings:(id)sender;
- (IBAction)testFunction:(id)sender;

@end


